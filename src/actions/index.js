import axios from 'axios';

export const CREATE_POST = 'CREATE_POST';
export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POST = 'FETCH_POST';
export const DELETE_POST = 'DELETE_POST';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';

// Here be serious with the custom api key you want to use
// The api's owner will close it if you put a api key
// with childish stuff. You know what I mean, please be focus and serious on this.
const API_KEY = '?key=AlonsoIsLearningWithBitBucket'; 


export function createPost(props) {
  const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, props);

  return {
    type: CREATE_POST,
    payload: request
  };
}

export function fetchPosts() {
  const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);
 
  return {
    type: FETCH_POSTS,
    payload: request
  };
 }

 export function fetchPost(id) {
   const request = axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`);

   return {
     type: FETCH_POST,
     payload: request
   }
 }

 export function deletePost(id) {
   const request = axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`);

   return {
     type: DELETE_POST,
     payload: request
   }
 }